public class TrainCar {
	public WildCat cat;
	public TrainCar next;
    public static final double EMPTY_WEIGHT = 20; 

    

    public TrainCar(WildCat cat) {
        this.cat = cat;
		
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
		this.next = next;
		
    }

    public double computeTotalWeight() {
        if (next == null){
			return cat.weight + EMPTY_WEIGHT;
		}else{
		    return cat.weight + EMPTY_WEIGHT + next.computeTotalWeight();
		}
    }

    public double computeTotalMassIndex() {
        if(next == null){
			return cat.computeMassIndex();
		} else{
			return cat.computeMassIndex() + next.computeTotalMassIndex();
		}
    }

    public void printCar() {
        System.out.printf("--(%s)  " , cat.nama);
		if(next == null){
			System.out.print("");
		}
		else{
			next.printCar();
		}
    }
}
