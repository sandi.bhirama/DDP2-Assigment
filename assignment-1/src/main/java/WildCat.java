public class WildCat{
	public String nama;
	public double weight;
	public double length;
	
	public WildCat(String nama , double weight , double length){
		this.nama = nama;
		this.weight = weight;
		this.length = length;
	}
	
	public double computeMassIndex(){
		double bmi = this.weight * 10000 / (this.length*this.length);
		return bmi;
	}
}