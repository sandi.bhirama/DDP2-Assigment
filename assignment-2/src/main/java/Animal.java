import java.util.*;
import java.lang.*;

public class Animal {
	protected String name;
	protected int bodyLength;
	protected int length, width;
	protected char tipe;
	protected boolean type;//false = indoor, true = outdoor
	
	public Animal(String name, int bodyLength, boolean type){
		this.name = name;
		this.bodyLength = bodyLength;
		this.type = type;
		if (type == false){
			if (bodyLength <= 45){
				this.length = 60;
				this.width = 60;
				this.tipe = 'A';
			}
			else if (bodyLength < 60){
				this.length = 60;
				this.width = 90;
				this.tipe = 'B';
			}
			else{
				this.length = 60;
				this.width = 120;
				this.tipe = 'C';
			}
		}
		else {
			if (bodyLength <= 75){
				this.length = 120;
				this.width = 120;
				this.tipe = 'A';
			}
			else if (bodyLength < 90){
				this.length = 120;
				this.width = 150;
				this.tipe = 'B';
			}
			else{
				this.length = 120;
				this.width = 180;
				this.tipe = 'C';
			}
		}
	}
}
	