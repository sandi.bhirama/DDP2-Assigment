import java.util.*;
import java.lang.*;

public class Cat extends Animal {
	public static int banyakCat;
	public static ArrayList<Cat> daftarCat = new ArrayList<Cat>();
	
	public static void arrangement(){
		if (banyakCat == 0)return;
		Cat[][] level = new Cat[4][];
		int banyakNormal = banyakCat / 3;
		int sisaNormal = banyakCat % 3;
		if (sisaNormal == 0){
			level[3] = new Cat[banyakNormal];
			level[2] = new Cat[banyakNormal];
			level[1] = new Cat[banyakNormal];
		}
		else if (sisaNormal == 1){
			level[3] = new Cat[banyakNormal + 1];
			level[2] = new Cat[banyakNormal];
			level[1] = new Cat[banyakNormal];
		}
		else if (sisaNormal == 2){
			level[3] = new Cat[banyakNormal + 1];
			level[2] = new Cat[banyakNormal + 1];
			level[1] = new Cat[banyakNormal];
		}
		int i = 0;
		for(int dummy = 1; dummy <= 3; dummy++)for(int j = 0; j < level[dummy].length; j++)level[dummy][j] = daftarCat.get(i++);
		System.out.println("location: indoor");
		for(int dummy = 3; dummy >= 1; dummy--){
			System.out.print("level " + dummy + ":");
			for(int j = 0; j < level[dummy].length; j++)
				System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
			System.out.println();
		}
		System.out.println();
		System.out.println("After rearrangement..."); 
		
		int dummy = 2;
		System.out.print("level " + 3 + ":");
		for(int j = level[dummy].length - 1; j >= 0; j--)
			System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
		System.out.println();
		
		dummy = 1;
		System.out.print("level " + 2 + ":");
		for(int j = level[dummy].length - 1; j >= 0; j--)
			System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
		System.out.println();
		
		dummy = 3;
		System.out.print("level " + 1 + ":");
		for(int j = level[dummy].length - 1; j >= 0; j--)
			System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
		System.out.println("\n");
	}	
	
	
	public Cat(String nama, int bodyLength){
		super(nama, bodyLength, false);
	}
	
	public String bersuara(int pick){
		if (pick == 1){
			System.out.println("Time to clean " + name + "'s fur");
			String suara = "Nyaaan...";
			return name + " makes a voice: " + suara;
		}
		if (pick == 2){
			Random rand = new Random();
			int i = rand.nextInt(4);
			String suara;
			if (i == 0) suara = "Miaaaw..";
			else if (i == 1) suara = "Purrr..";
			else if (i == 2) suara = "Mwaw!";
			else suara = "Mraaawr!";
			return name + " makes a voice: " + suara;
		}
		return null;
	}
	
	public void terpilih(){
		System.out.println("You are visiting " + name + " (cat) now, what would you like to do?");
		System.out.println("1: Brush the fur 2: Cuddle");
		Scanner in = new Scanner(System.in);
		String inputan = in.nextLine();
		int pick = Integer.parseInt(inputan.substring(2, inputan.length() - 2));
		System.out.println(bersuara(pick));
		System.out.println("Back to the office!");
		System.out.println();
	}
}