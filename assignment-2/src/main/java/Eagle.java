import java.util.*;
import java.lang.*;

public class Eagle extends Animal {
	public static int banyakEagle;
	public static ArrayList<Eagle> daftarEagle = new ArrayList<Eagle>();
	
	public static void arrangement(){
		if (banyakEagle == 0)return;
		int banyakCat = banyakEagle;
		ArrayList<Eagle> daftarCat = daftarEagle;
		Eagle[][] level = new Eagle[4][];
		int banyakNormal = banyakCat / 3;
		int sisaNormal = banyakCat % 3;
		if (sisaNormal == 0){
			level[3] = new Eagle[banyakNormal];
			level[2] = new Eagle[banyakNormal];
			level[1] = new Eagle[banyakNormal];
		}
		else if (sisaNormal == 1){
			level[3] = new Eagle[banyakNormal + 1];
			level[2] = new Eagle[banyakNormal];
			level[1] = new Eagle[banyakNormal];
		}
		else if (sisaNormal == 2){
			level[3] = new Eagle[banyakNormal + 1];
			level[2] = new Eagle[banyakNormal + 1];
			level[1] = new Eagle[banyakNormal];
		}
		int i = 0;
		for(int dummy = 1; dummy <= 3; dummy++)for(int j = 0; j < level[dummy].length; j++)level[dummy][j] = daftarCat.get(i++);
		System.out.println("location: outdoor");
		for(int dummy = 3; dummy >= 1; dummy--){
			System.out.print("level " + dummy + ":");
			for(int j = 0; j < level[dummy].length; j++)
				System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
			System.out.println();
		}
		System.out.println();
		System.out.println("After rearrangement..."); 
		
		int dummy = 2;
		System.out.print("level " + 3 + ":");
		for(int j = level[dummy].length - 1; j >= 0; j--)
			System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
		System.out.println();
		
		dummy = 1;
		System.out.print("level " + 2 + ":");
		for(int j = level[dummy].length - 1; j >= 0; j--)
			System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
		System.out.println();
		
		dummy = 3;
		System.out.print("level " + 1 + ":");
		for(int j = level[dummy].length - 1; j >= 0; j--)
			System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
		System.out.println("\n");
	}	
	
	public Eagle(String nama, int bodyLength){
		super(nama, bodyLength, true);
	}
	
	public String bersuara(int pick){
		if (pick == 1){
			String suara = "Kwaakk....";
			return name + " makes a voice: " + suara + "\n" + "You hurt!";
		}
		else {
			String suara = "You do nothing...";
			return suara;
		}
	}
	
	public void terpilih(){
		System.out.println("You are visiting " + name + " (eagle) now, what would you like to do?");
		System.out.println("1: Order to fly");
		Scanner in = new Scanner(System.in);
		String inputan = in.nextLine();
		int pick = Integer.parseInt(inputan.substring(2, inputan.length() - 2));
		System.out.println(bersuara(pick));
		System.out.println("Back to the office!");
		System.out.println();
	}
}