import java.util.*;
import java.lang.*;

public class Lion extends Animal {
	public static int banyakLion;
	public static ArrayList<Lion> daftarLion = new ArrayList<Lion>();
	
	public Lion(String nama, int bodyLength){
		super(nama, bodyLength, true);
	}
	
	public static void arrangement(){
		if (banyakLion == 0)return;
		int banyakCat = banyakLion;
		ArrayList<Lion> daftarCat = daftarLion;
		Lion[][] level = new Lion[4][];
		int banyakNormal = banyakCat / 3;
		int sisaNormal = banyakCat % 3;
		if (sisaNormal == 0){
			level[3] = new Lion[banyakNormal];
			level[2] = new Lion[banyakNormal];
			level[1] = new Lion[banyakNormal];
		}
		else if (sisaNormal == 1){
			level[3] = new Lion[banyakNormal + 1];
			level[2] = new Lion[banyakNormal];
			level[1] = new Lion[banyakNormal];
		}
		else if (sisaNormal == 2){
			level[3] = new Lion[banyakNormal + 1];
			level[2] = new Lion[banyakNormal + 1];
			level[1] = new Lion[banyakNormal];
		}
		int i = 0;
		for(int dummy = 1; dummy <= 3; dummy++)for(int j = 0; j < level[dummy].length; j++)level[dummy][j] = daftarCat.get(i++);
		System.out.println("location: outdoor");
		for(int dummy = 3; dummy >= 1; dummy--){
			System.out.print("level " + dummy + ":");
			for(int j = 0; j < level[dummy].length; j++)
				System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
			System.out.println();
		}
		System.out.println();
		System.out.println("After rearrangement..."); 
		
		int dummy = 2;
		System.out.print("level " + 3 + ":");
		for(int j = level[dummy].length - 1; j >= 0; j--)
			System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
		System.out.println();
		
		dummy = 1;
		System.out.print("level " + 2 + ":");
		for(int j = level[dummy].length - 1; j >= 0; j--)
			System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
		System.out.println();
		
		dummy = 3;
		System.out.print("level " + 1 + ":");
		for(int j = level[dummy].length - 1; j >= 0; j--)
			System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
		System.out.println("\n");
	}	
	
	public String bersuara(int pick){
		if (pick == 1){
			System.out.println("Lion is hunting..");
			String suara = "err....";
			return name + " makes a voice: " + suara;
		}
		if (pick == 2){
			System.out.println("Clean the lion's mane..");
			String suara = "Hauhhmm!";
			return name + " makes a voice: " + suara;
		}
		if (pick == 3){
			String suara = "HAAHUM!!";
			return name + " makes a voice: " + suara;
		}
		return null;
	}
	
	public void terpilih(){
		System.out.println("You are visiting " + name + " (lion) now, what would you like to do?");
		System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
		Scanner in = new Scanner(System.in);
		String inputan = in.nextLine();
		int pick = Integer.parseInt(inputan.substring(2, inputan.length() - 2));
		System.out.println(bersuara(pick));
		System.out.println("Back to the office!");
		System.out.println();
	}
}