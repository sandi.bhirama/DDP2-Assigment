import java.util.*;
import java.lang.*;

public class MainCode {
	public static int stringToInt(String inputan){
		int num = Integer.parseInt(inputan.substring(2, inputan.length() - 2));
		return num;
	}
	
	public static String[] stringToArrStr(String inputan){
		String temp = inputan.substring(2,inputan.length() - 2);
		String[] arrTemp = temp.split(",");
		String[] hasil = new String[arrTemp.length];
		for(int i = 0; i < arrTemp.length; i++){
			int awal = 0, akhir = 0;
			while (akhir != arrTemp[i].length() && arrTemp[i].charAt(akhir) != '|')akhir++;
			String nama = arrTemp[i].substring(awal, akhir);
			hasil[i] = nama;
		}
		return hasil;
	}
	
	public static int[] stringToArrInt(String inputan){
		String temp = inputan.substring(2,inputan.length() - 2);
		String[] arrTemp = temp.split(",");
		int[] hasil = new int[arrTemp.length];
		for(int i = 0; i < arrTemp.length; i++){
			int awal = 0, akhir = 0;
			while (akhir != arrTemp[i].length() && arrTemp[i].charAt(akhir) != '|')akhir++;
			String umure = arrTemp[i].substring(akhir + 1, arrTemp[i].length());
			int umur = Integer.parseInt(umure);
			hasil[i] = umur;
		}
		return hasil;
	}
	
	public void proses(int type){
		String tipeBin = "";
		if (type == 0)tipeBin = "cat";
		else if (type == 1)tipeBin = "lion";
		else if (type == 2)tipeBin = "eagle";
		else if (type == 3)tipeBin = "parrot";
		else if (type == 4)tipeBin = "hamster";
		System.out.print(tipeBin + ": ");
		Scanner in = new Scanner(System.in);
		String str = in.nextLine();
		int strInt = stringToInt(str);
		if (type == 0)Cat.banyakCat = strInt;
		else if (type == 1)Lion.banyakLion = strInt;
		else if (type == 2)Eagle.banyakEagle = strInt;
		else if (type == 3)Parrot.banyakParrot = strInt;
		else if (type == 4)Hamster.banyakHamster = strInt;
		if (strInt != 0){
			System.out.println("Provide the information of " + tipeBin + "(s):");
			String temp = in.nextLine();
			int[] hasilUmur = stringToArrInt(temp); 
			String[] hasilNama = stringToArrStr(temp);
			for(int i = 0; i < strInt; i++){
				if (type == 0)Cat.daftarCat.add(new Cat(hasilNama[i], hasilUmur[i]));
				else if (type == 1)Lion.daftarLion.add(new Lion(hasilNama[i], hasilUmur[i]));
				else if (type == 2)Eagle.daftarEagle.add(new Eagle(hasilNama[i], hasilUmur[i]));
				else if (type == 3)Parrot.daftarParrot.add(new Parrot(hasilNama[i], hasilUmur[i]));
				else if (type == 4)Hamster.daftarHamster.add(new Hamster(hasilNama[i], hasilUmur[i]));
			}
		}
	}
	
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Welcome to Javari Park!");
		System.out.println("Input the number of animals");
		MainCode dummy = new MainCode();
		for(int i = 0; i < 5; i++)dummy.proses(i);
		//SELESAI
		System.out.println("Animals have been successfully recorded!\n\n=============================================");
		//CAGE
		System.out.println("Cage arrangement:");
		Cat.arrangement();
		Lion.arrangement();
		Eagle.arrangement();
		Parrot.arrangement();
		Hamster.arrangement();
		System.out.println("NUMBER OF ANIMALS:");
		System.out.println("cat:" + Cat.banyakCat);
		System.out.println("lion:" + Lion.banyakLion);
		System.out.println("parrot:" + Parrot.banyakParrot);
		System.out.println("eagle:"+ Eagle.banyakEagle);
		System.out.println("hamster:" + Hamster.banyakHamster);
		System.out.println();
		System.out.println("=============================================");
		while(true){
			System.out.println("Which animal you want to visit?");
			System.out.println("1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit");
			int pick = stringToInt(in.nextLine());
			String namanya = "";
			if (pick == 1){
				System.out.print("Mention the name of cat you want to visit: ");
				namanya = stringToArrStr(in.nextLine())[0];
				
				boolean ada = false;
				for(int i = 0; i < Cat.daftarCat.size(); i++)if (namanya.equals(Cat.daftarCat.get(i).name)){
					ada = true;
					Cat.daftarCat.get(i).terpilih();
					break;
				}
				if (!ada){
					System.out.println("There is no cat with that name! Back to the office!\n");
					continue;
				}
			}
			else if (pick == 2){
				System.out.print("Mention the name of eagle you want to visit: ");
				namanya = stringToArrStr(in.nextLine())[0];
				
				boolean ada = false;
				for(int i = 0; i < Eagle.daftarEagle.size(); i++)if (namanya.equals(Eagle.daftarEagle.get(i).name)){
					ada = true;
					Eagle.daftarEagle.get(i).terpilih();
					break;
				}
				if (!ada){
					System.out.println("There is no eagle with that name! Back to the office!\n");
					continue;
				}
			}
			else if (pick == 3){
				System.out.print("Mention the name of hamster you want to visit: ");
				namanya = stringToArrStr(in.nextLine())[0];
				
				boolean ada = false;
				for(int i = 0; i < Hamster.daftarHamster.size(); i++)if (namanya.equals(Hamster.daftarHamster.get(i).name)){
					ada = true;
					Hamster.daftarHamster.get(i).terpilih();
					break;
				}
				if (!ada){
					System.out.println("There is no hamster with that name! Back to the office!\n");
					continue;
				}
			}
			else if (pick == 4){
				System.out.print("Mention the name of parrot you want to visit: ");
				namanya = stringToArrStr(in.nextLine())[0];
				
				boolean ada = false;
				for(int i = 0; i < Parrot.daftarParrot.size(); i++)if (namanya.equals(Parrot.daftarParrot.get(i).name)){
					ada = true;
					Parrot.daftarParrot.get(i).terpilih();
					break;
				}
				if (!ada){
					System.out.println("There is no parrot with that name! Back to the office!\n");
					continue;
				}
			}
			else if (pick == 5){
				System.out.print("Mention the name of lion you want to visit: ");
				namanya = stringToArrStr(in.nextLine())[0];
				
				boolean ada = false;
				for(int i = 0; i < Lion.daftarLion.size(); i++)if (namanya.equals(Lion.daftarLion.get(i).name)){
					ada = true;
					Lion.daftarLion.get(i).terpilih();
					break;
				}
				if (!ada){
					System.out.println("There is no lion with that name! Back to the office!\n");
					continue;
				}
			}
			else break;
		}
	}
}