import java.util.*;
import java.lang.*;

public class Parrot extends Animal {
	public static int banyakParrot;
	public static ArrayList<Parrot> daftarParrot = new ArrayList<Parrot>();
	
	public static void arrangement(){
		if (banyakParrot == 0)return;
		int banyakCat = banyakParrot;
		ArrayList<Parrot> daftarCat = daftarParrot;
		Parrot[][] level = new Parrot[4][];
		int banyakNormal = banyakCat / 3;
		int sisaNormal = banyakCat % 3;
		if (sisaNormal == 0){
			level[3] = new Parrot[banyakNormal];
			level[2] = new Parrot[banyakNormal];
			level[1] = new Parrot[banyakNormal];
		}
		else if (sisaNormal == 1){
			level[3] = new Parrot[banyakNormal + 1];
			level[2] = new Parrot[banyakNormal];
			level[1] = new Parrot[banyakNormal];
		}
		else if (sisaNormal == 2){
			level[3] = new Parrot[banyakNormal + 1];
			level[2] = new Parrot[banyakNormal + 1];
			level[1] = new Parrot[banyakNormal];
		}
		int i = 0;
		for(int dummy = 1; dummy <= 3; dummy++)for(int j = 0; j < level[dummy].length; j++)level[dummy][j] = daftarCat.get(i++);
		System.out.println("location: indoor");
		for(int dummy = 3; dummy >= 1; dummy--){
			System.out.print("level " + dummy + ":");
			for(int j = 0; j < level[dummy].length; j++)
				System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
			System.out.println();
		}
		System.out.println();
		System.out.println("After rearrangement..."); 
		
		int dummy = 2;
		System.out.print("level " + 3 + ":");
		for(int j = level[dummy].length - 1; j >= 0; j--)
			System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
		System.out.println();
		
		dummy = 1;
		System.out.print("level " + 2 + ":");
		for(int j = level[dummy].length - 1; j >= 0; j--)
			System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
		System.out.println();
		
		dummy = 3;
		System.out.print("level " + 1 + ":");
		for(int j = level[dummy].length - 1; j >= 0; j--)
			System.out.print(" " + level[dummy][j].name + " (" + level[dummy][j].bodyLength + " - " + level[dummy][j].tipe + "),");
		System.out.println("\n");
	}	
	
	public Parrot(String nama, int bodyLength){
		super(nama, bodyLength, false);
	}
	
	public String bersuara(int pick){
		if (pick == 1){
			String suara = "FLYYYY...";
			return "Parrot " + name + " flies!" + name + " makes a voice: " + suara;
		}
		if (pick == 2){
			Scanner in = new Scanner(System.in);
			System.out.print("You say: ");
			String suaraManusia[] = MainCode.stringToArrStr(in.nextLine());
			String suara = suaraManusia[0].toUpperCase();
			return name + " makes a voice: " + suara;
		}
		return null;
	}
	
	public void terpilih(){
		System.out.println("You are visiting " + name + " (parrot) now, what would you like to do?");
		System.out.println("1: Order to fly 2: Do conversation");
		Scanner in = new Scanner(System.in);
		String inputan = in.nextLine();
		int pick = Integer.parseInt(inputan.substring(2, inputan.length() - 2));
		System.out.println(bersuara(pick));
		System.out.println("Back to the office!");
		System.out.println();
	}
}