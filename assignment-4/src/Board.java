

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.util.List;
import java.awt.Dimension;
import java.util.Collections;


public class Board extends JFrame implements ActionListener {

    private JPanel cardPanel;
    private JPanel resetExitPanel;
    private JButton restartButton = new JButton("Play Again ?");
    private JButton exitButton = new JButton("Exit");
    private List<Card> cardList;
    
    public static Card selected1 = null;
    public static Card selected2 = null;
    public static boolean alreadyWin = false;
    public static int totalAttempt = 0;

    public static JLabel attemptLabel = new JLabel("Attempt : " + totalAttempt);
    
    public Board(String title, List<Card> cardList) {
        super(title);
        this.cardList = cardList;
        this.cardPanel = new JPanel(new GridLayout(6,6));
        this.resetExitPanel = new JPanel(new GridLayout(1,2));
        this.restartButton.addActionListener(this);
        this.restartButton.setActionCommand("Restart");
        this.exitButton.addActionListener(this);
        this.exitButton.setActionCommand("Exit");
        
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(true);
        this.addCard(cardList);
        this.addCardPanel();
        this.resetExitToPanel();
        this.addResetExitAttemptPanel();
        this.setSize(620,700);
        this.setVisible(true);
        this.setLocation(600,150);

   
    }


    public void resetExitToPanel() {
        restartButton.setPreferredSize(new Dimension(60,50));
        exitButton.setPreferredSize(new Dimension(60,50));
        resetExitPanel.add(restartButton, 0);
        resetExitPanel.add(exitButton, 1);
        //resetPanel.add(attemptLabel, 2);
    }

    public void addCard(List<Card> cardList) {
        for(Card card : cardList) {
            cardPanel.add(card);
        }
    }

    
    public void addCardPanel() {
        this.add(cardPanel, BorderLayout.PAGE_START);
    }

    public void addResetExitAttemptPanel() {
        this.add(resetExitPanel, BorderLayout.CENTER);
        this.add(Board.attemptLabel, BorderLayout.SOUTH);
        this.setBackground(Color.BLACK);

    }

    public void restart() {
    	 Collections.shuffle(cardList);
         for(Card card : cardList) {
             card.setIsSelected(false);
             card.setIcon(Card.background);
             card.setEnabled(true);
         }
         addCard(cardList);
         addCardPanel();
         totalAttempt = 0;
         Board.attemptLabel.setText("Attempt: " + totalAttempt);
    }

    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();
        if(action.equals("Restart")) {
            restart();
        }
        else if(action.equals("Exit")) {
            System.exit(0);
        }

    }
    


}