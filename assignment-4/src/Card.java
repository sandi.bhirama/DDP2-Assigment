

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.Timer;

public class Card extends JButton implements ActionListener {
    public static final Icon background = new ImageIcon("src\\marvel.png");
    private boolean isSelected = false;
    private Icon mainImage;
    private int index;
    private Timer transition;
    
    

    public Card(int index, String mainImage) {
        super(background);
        this.index = index;
        this.mainImage = new ImageIcon(mainImage);
        Image rescaled = ((ImageIcon)this.mainImage).getImage().getScaledInstance(90,90, java.awt.Image.SCALE_SMOOTH);
        this.mainImage = new ImageIcon(rescaled);
        this.setPreferredSize(new Dimension(100,100));
        this.addActionListener(this);
        this.setBackground(Color.WHITE);
        transition = new Timer(300, new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                if(Board.selected1.isMatched(Board.selected2)) {
                    Board.selected1.match(Board.selected2);
                } else {
                    Board.selected1.notMatch(Board.selected2);
                }
            }
        });
        transition.setRepeats(false);
    }

    public int getIndex() {
        return this.index;
    }

    public void setIsSelected(boolean arg) {
        this.isSelected = arg;
    }

    public boolean isMatched(Card selected2) {
        if(this.index == selected2.getIndex()) return true;
        else return false;
    }

    public void match(Card selected2) {
        this.setEnabled(false);
        selected2.setEnabled(false);
        Board.selected1 = null;
        Board.selected2 = null;
    }

    public void notMatch(Card selected2) {
        this.setIcon(background);
        selected2.setIcon(background);
        isSelected = false;
        selected2.setIsSelected(false); 
        Board.selected1 = null;
        Board.selected2 = null;
    }

    public void pairing() {
        if(Board.selected1 != null && Board.selected2 != null) {
        	Board.totalAttempt = ++ Board.totalAttempt;
            Board.attemptLabel.setText("Attempt: " + Board.totalAttempt);
            transition.start();
        }
    }

    public void actionPerformed(ActionEvent e) {
        if(!isSelected) {
            this.setIcon(mainImage);
            isSelected = true;
            if(Board.selected1 == null) Board.selected1 = this;
            else if(Board.selected2 == null) Board.selected2 = this;
            pairing();
        }     
        
        
    }
}