

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;;

 public class Game {

    public static void main(String[] args) {
        List<Card> cardList = new ArrayList<Card>();
        String[] imageString = {"src\\1.png", "src\\2.png", "src\\3.png", "src\\4.png","src\\5.png", "src\\6.png",
                                "src\\7.png", "src\\8.png","src\\9.png", "src\\10.png", "src\\11.png", "src\\12.png",
                                "src\\13.png", "src\\14.png", "src\\15.png", "src\\16.png", "src\\17.png", "src\\18.jpg"};
        
        for(int a = 0; a < imageString.length; a++) {
            cardList.add(new Card(a, imageString[a]));
            cardList.add(new Card(a, imageString[a]));
        }
        Collections.shuffle(cardList);
        Board board = new Board("Remember The Avengers", cardList);
    }
 }